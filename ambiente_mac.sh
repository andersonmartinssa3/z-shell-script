#!/bin/sh

#instalar oh my zsh
echo "instalando oh my zsh..."
sh -c "$(curl -fsSL https://raw.github.com/ohmyzsh/ohmyzsh/master/tools/install.sh)"

#permissao oh my zsh
echo "Adicionando permissao para zsh..."
compaudit | xargs chmod g-w,o-w /usr/local/share/zsh
compaudit | xargs chmod g-w,o-w /usr/local/share/zsh/site-functions

#instalacao Homebrew
echo "Instalando Homebrew..."
/bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"

#instalacao jdk-8
echo "Instalando jdk-8..."
brew tap adoptopenjdk/openjdk
brew install --cask adoptopenjdk8

#instalacao maven
echo "instalando maven..."
brew install maven

#instalacao vs code
echo "Instalando vscode..."
brew install --cask visual-studio-code

#instalacao android studio
echo "Instalando android studio..."
brew install --cask android-studio

#flutter
echo "Instalando flutter..."
cd $HOME
mkdir tools
cd tools
 git clone https://github.com/flutter/flutter.git -b stable

#SLIDY

#XCode
echo "xcode..."
sudo xcode-select --switch /Applications/Xcode.app/Contents/Developer
sudo xcodebuild -runFirstLaunch

echo "Instalando cocoapods..."
sudo gem install cocoapods

sudo arch -x86_64 gem install ffi
arch -x86_64 pod install

#NVM
brew install nvm
mkdir ~/.nvm 
echo "HABILITAR EXPORT .ZSHRC"
