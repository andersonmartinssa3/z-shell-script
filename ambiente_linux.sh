#!/bin/sh

printf $HOME
cd $HOME

folder_repository="repository"
folder_tools="tools"
folder_temp="temp_install"

printf "/n/n/nCriando pasta repository..."
mkdir folder_repository

printf "/n/n/nCriando pasta tools..."
mkdir folder_tools

printf "/n/n/nCriando pasta temp para instalacao..."
mkdir folder_temp

printf "/n/n/nCopiando arquivos para pasta $folder_temp..."
cp zshrc folder_temp/.zshrc

#atualiza a lista de pacotes e programas que podem ser instalados na máquina.
sudo apt-get update

#atualiza o sistema e baixa e instala atualizações de pacotes e dos programas da máquina.
sudo apt-get upgrade

#JDK-8
printf "/n/n/nInstalando jdk-8..."
sudo apt -y install openjdk-8-jdk openjdk-8-jre

#GIT
printf "/n/n/nInstalando git..."
sudo apt-get -y install git

#ZSHELL E Oh-My-ZSH
printf "/n/n/nInstalando z shell..."
sudo apt-get -y install zsh

printf "/n/n/nDefinido z shell como padrao..."
whereis zsh
sudo usermod -s /usr/bin/zsh $(whoami)

printf "/n/n/nInstalando Oh-My-ZSH..."
sh -c "$(wget https://raw.githubusercontent.com/robbyrussell/oh-my-zsh/master/tools/install.sh -O -)"
    
cd folder_temp
mv .zshrc ~/.zshrc
cd..

# JetBrains Toolbox
printf "/n/n/nInstalando JetBrains Toolbox..."
cd folder_tools
wget -cO jetbrains-toolbox.tar.gz "https://data.services.jetbrains.com/products/download?platform=linux&code=TBA"
tar -xzf jetbrains-toolbox.tar.gz
DIR=$(find . -maxdepth 1 -type d -name jetbrains-toolbox-\* -print | head -n1)
cd $DIR
./jetbrains-toolbox
cd ..
rm -r $DIR
rm jetbrains-toolbox.tar.gz
cd ..

#VS CODE
printf "/n/n/nInstalando vs code..."
sudo snap install --classic code

#FLUTTER
cd folder_tools
git clone https://github.com/flutter/flutter.git -b stable
cd ..
flutter doctor

#PERGUNTA PARA REINICAR O COMPUTADOR
reboot () { printf 'Deseja reiniciar? (y/n)' && read x &&  [[ "$x" == "y" ]] && /sbin/reboot }