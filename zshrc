# If you come from bash you might have to change your $PATH.
#export PATH=$HOME/bin:/usr/local/bin:$PATH

os=""
mac="mac"
linux="linux"
case `uname` in
  Darwin)
    os=mac
  ;;
  Linux)
    os=linux
  ;;
esac

username="anderson"
tools_folder="tools"
repository_folder="repository"

# Path to your oh-my-zsh installation.
if [ "$os" = "$mac" ]; then
	export ZSH="/Users/$username/.oh-my-zsh"
else
	export ZSH="/home/$username/.oh-my-zsh"
fi

DOCUMENTS=""
if [ "$os" = "$mac" ]; then
    DOCUMENTS="$HOME/Documents"
else
	DOCUMENTS="$HOME/Documentos"
fi

#FLUTTER
export PATH="$DOCUMENTS/$tools_folder/flutter/bin:$PATH"
export PATH="$PATH":"$DOCUMENTS/$tools_folder/flutter/.pub-cache/bin"
export PATH="$PATH":"$DOCUMENTS/$tools_folder/flutter/bin/cache/dart-sdk/bin"
export PATH="$PATH":"$HOME/.pub-cache/bin"

#JAVA
if [ "$os" = "$mac" ]; then
	export JAVA_HOME=$(/usr/libexec/java_home)
else
	export JAVA_HOME=/usr/lib/jvm/java-8-openjdk-amd64
	export JRE_HOME=/usr/lib/jvm/java-8-openjdk-amd64/jre
fi

#MAVEN
export M3_HOME="/usr/share/maven"
export MAVEN_HOME="/usr/share/maven"
export PATH="${M2_HOME}/bin:${PATH}"

#NVM
#export NVM_DIR=~/.nvm
#source $(brew --prefix nvm)/nvm.sh

ZSH_THEME="frisk"

plugins=(
	git
	colored-man-pages
	bgnotify
)

SPACESHIP_PROMPT_SYMBOL="➜"

source $ZSH/oh-my-zsh.sh

alias repository="cd $DOCUMENTS/$repository_folder"
alias config_terminal="code ~/.zshrc"

alias mobx_build_clean="slidy run mobx_build_clean"
alias mobx="slidy run mobx_build"

if [ "$os" = "$linux" ]; then
	alias config_acento='setxkbmap -model abnt -layout us -variant intl'
fi

flutter_test() {
	cd test
	flutter test
	cd ..
}

flutter_test_coverage() {
	cd test
	flutter test --coverage
	cd ..
	genhtml -o coverage coverage/lcov.info
	google-chrome coverage/index.html
}

new_branch() {
	if [ -z "$1" ]; then
		echo "nome da branch obrigatorio"
		return 0;
	fi
	
	echo "Criando branch $1..."
	git checkout develop
	git fetch -p
	git pull
	git checkout -b "$1"
	git push -f -u origin "$1"
}

commit() {
	if [ -z "$1" ]; then
		echo "mensagem do commit obrigatorio"
		return 0;
	fi
	
	echo "commit $1"
	git add .
	git commit -m "$1"
	git push
}

rebase() {
	if [ -z "$1" ]; then
		echo "nome da branch obrigatorio"
		return 0;
	fi
	
	echo "rebase $1"
	git rebase -i "$1"
}

pull() {
	git fetch -p
	git pull
}

develop(){
	git checkout develop
}

delete_all_branch() {
	echo "deletando todas as branchs menos a DEVELOP e a MASTER..."
	git branch | grep -v "develop" | grep -v "master" | xargs git branch -D	
}

getOS() {
	echo $os
}

startPreVendaApi() {
	cd "$DOCUMENTS/$repository_folder/pre venda/prevenda-api/"
	docker-compose up -d
}

stopPreVendaApi() {
	cd "$DOCUMENTS/$repository_folder/pre venda/prevenda-api/"
	docker-compose stop
}

flutter_run_web() {
	flutter run -d chrome --no-sound-null-safety --web-renderer=html
}

flutter_deploy_web() {
	flutter build web --no-sound-null-safety --web-renderer html --release
	firebase deploy --only hosting
}

flutter_run() {
	flutter run --no-sound-null-safety
}

create_key_ssh() {
	ssh-keygen
	eval `ssh-agent`
	ssh-add ~/.ssh/id_rsa
	cat ~/.ssh/id_rsa.pub
	echo "SSH criado e copiado!"
}

